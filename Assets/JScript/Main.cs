﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public GameObject login;
    public GameObject regi;
    public GameObject regi_su;
    public GameObject regi_fail;

    public GameObject map;
    public GameObject info;
    public GameObject player_pos;
    public GameObject can;

    public GameObject point;
    public GameObject parte_1;
    public GameObject parte_2;
    public GameObject parte_3;
    public GameObject parte_4;

    public GameObject 口湖鄉;
    public GameObject 水林鄉;
    public GameObject 北港鎮;
    public GameObject 四湖鄉;
    float posx = -250;
    float posy = -250;

    public GameObject[] pointlist;
    GameObject point_cn;

    public int q_ramdon = 0;

    public InputField name;
    public Text sex;
    public Text name_1;
    public Text sex_1;

    public GameObject boy_pic;
    public GameObject girl_pic;

    // Start is called before the first frame update
    void Start()
    {
        q_ramdon = PlayerPrefs.GetInt("q_ramdon");

        point_fn(23.50f, 120.135f, parte_1);
        point_fn(23.50f, 120.195f, parte_2);
        point_fn(23.52f, 120.26f, parte_3);
        point_fn(23.55f, 120.145f, parte_4);

        
        if (q_ramdon == 0)
        {
            q_ramdon = Random.RandomRange(1, 3);
            PlayerPrefs.SetInt("q_ramdon", q_ramdon);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (口湖鄉.activeSelf)
        {
            player_pos.transform.SetParent(parte_1.transform);
        }
        if (水林鄉.activeSelf)
        {
            player_pos.transform.SetParent(parte_2.transform);
        }
        if (北港鎮.activeSelf)
        {
            player_pos.transform.SetParent(parte_3.transform);
        }
        if (四湖鄉.activeSelf)
        {
            player_pos.transform.SetParent(parte_4.transform);
        }
        
        pointlist = GameObject.FindGameObjectsWithTag("pos");
    }
    public void login_fn()
    {
        login.SetActive(false);
        map.SetActive(true);
    }
    public void register_fn()
    {
        regi.SetActive(false);
        regi_su.SetActive(true);

        name_1.text = name.text;
        sex_1.text = sex.text;
        if (sex_1.text == "男")
        {
            boy_pic.SetActive(true);
        }
        if (sex_1.text == "女")
        {
            girl_pic.SetActive(true);
        }
    }
    public void info_fn()
    {
        info.active = !info.active;
    }
    public void reset_pos()
    {
        player_pos.transform.localPosition = new Vector3(-700, -250, 0);
    }
    
    public void point_fn(float aa, float bb, GameObject xx)
    {
        float la = aa;
        float lo = bb;

        for (int x = 0; x < 17; x++)
        {
            for (int y = 0; y < 19; y++)
            {
                point_cn = Instantiate(point, xx.transform);
                point_cn.transform.localPosition = new Vector3(posx, posy);
                posy = posy + 30;

                point_cn.GetComponent<point_SC>().latitude = la;
                point_cn.GetComponent<point_SC>().longitude = lo;
                la = la + 0.01f;
            }

            posx = posx + 30;
            posy = -250;
            la = aa;
            lo = lo + 0.005f;
        }
        posx = -250;
        posy = -250;
    }
}
