﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GPS : MonoBehaviour
{
    public Text latitude_t;
    public Text longitude_t;

    public float latitude;
    public float longitude;

    public Main main_sc;
    public bool opgps;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(once_fn());
    }

    // Update is called once per frame
    void Update()
    {
        
        latitude_t.text = latitude.ToString();
        longitude_t.text = longitude.ToString();

        if (opgps)
        {
            Input.location.Start();
            latitude = Input.location.lastData.latitude;
            longitude = Input.location.lastData.longitude;
        }
    }
    IEnumerator once_fn()
    {
        yield return new WaitForSeconds(1);
        for (int x = 0; x <= main_sc.pointlist.Length - 1; x++)
        {
            if (latitude <= main_sc.pointlist[x].GetComponent<point_SC>().latitude && longitude <= main_sc.pointlist[x].GetComponent<point_SC>().longitude)
            {
                //playerpos.transform.localPosition = this.transform.localPosition;
                main_sc.GetComponent<Main>().player_pos.transform.localPosition = main_sc.pointlist[x].transform.localPosition;
                Debug.Log(main_sc.pointlist[x].transform.localPosition);
                break;
            }
        }
        StartCoroutine(once_fn());
    }
    public void opengps_fn()
    {
        opgps = true;
    }
    public void closeapp_fn()
    {
        Input.location.Stop();
        Application.Quit();
    }
}
